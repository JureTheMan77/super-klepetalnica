// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

var preimenovanje=[["ime", "nadimek", "id"]];
Klepet.prototype.preslikaj=function(ime, id){
  for(var i=0;i<preimenovanje.length;i++){
    if(preimenovanje[i][0]==ime && preimenovanje[i][2]==-1){
      preimenovanje[i][2]=id;
    }
  }
  for(var j=0;j<preimenovanje.length;j++){
    if(preimenovanje[j][2]==id){
      return preimenovanje[j][1];
    }
  }
  return ime;
};

Klepet.prototype.prezrcali=function(ime){
  for(var i=0;i<preimenovanje.length;i++){
    if(preimenovanje[i][1]==ime){
      return preimenovanje[i][0];
    }
  }
  return ime;
};

/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      parametri[1]=Klepet.prototype.prezrcali(parametri[1]);
      
      if (parametri) {
        this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
        sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
    case 'preimenuj':
      var ime=besede[1];
      ime=ime.split('"').join('');
      var nadimek=besede[2];
      nadimek=nadimek.split('"').join('');
      var vsebuje=0;
      if(ime!=nadimek){
        for(var i=0;i<preimenovanje.length;i++){
          if(preimenovanje[i][0]==ime){
            preimenovanje[i][1]=nadimek;
            vsebuje=1;
            break;
          }
        }
        if(vsebuje==0){
          preimenovanje.push([ime, nadimek, -1]);
        }
      }
      break;
    case 'barva':
      //besede.shift();
      //var barva = besede.join(' ');
      var barva = besede[1];
      console.log(barva);
      $('#sporocila').css("background-color", barva);
      $('#kanal').css("background-color", barva);
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};